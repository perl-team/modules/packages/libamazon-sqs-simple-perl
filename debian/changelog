libamazon-sqs-simple-perl (2.07-2) UNRELEASED; urgency=medium

  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Fri, 21 Feb 2020 15:37:23 +0100

libamazon-sqs-simple-perl (2.07-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 11:41:54 +0100

libamazon-sqs-simple-perl (2.07-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Angel Abad ]
  * Import upstream version 2.07
  * debian/copyright: Update copyright years.
  * Bump debhelper compatibility level to 10.
  * Declare compliance with Debian Policy 4.2.1.

 -- Angel Abad <angel@debian.org>  Sun, 11 Nov 2018 09:19:22 +0100

libamazon-sqs-simple-perl (2.06-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Florian Schlichting ]
  * Import upstream version 2.06
  * Add upstream metadata
  * Add new (build-)dependencies on AWS::Signature4 and
    VM::EC2::Security::CredentialCache
  * Bump copyright years
  * Update license paragraphs to commonly used versions
  * Drop overrides and smoke-skip as the offending files are no longer shipped
  * Declare compliance with Debian Policy 4.1.1

 -- Florian Schlichting <fsfs@debian.org>  Sat, 11 Nov 2017 22:12:30 +0100

libamazon-sqs-simple-perl (2.04-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump debhelper compatibility level to 9.
  * Add build dependency on libtest-warn-perl.
  * debian/rules: recreate Makefile during clean.
  * Skip new development tests during build and autopkgtest.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Nov 2015 18:54:04 +0100

libamazon-sqs-simple-perl (2.03-2) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Declare conformance with Policy 3.9.6
  * mark package as autopkg-testable

 -- Damyan Ivanov <dmn@debian.org>  Sun, 22 Nov 2015 07:14:53 +0000

libamazon-sqs-simple-perl (2.03-1) unstable; urgency=low

  * Team upload.

  [ Angel Abad ]
  * debian/control: Add VCS- fields
  * Email change: Angel Abad -> angel@debian.org

  [ Fabrizio Regalli ]
  * Update d/compat to 8
  * Update debhelper to (>=8.0)

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update upstream contact and copyright holders.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Oct 2013 22:13:49 +0200

libamazon-sqs-simple-perl (1.06-1) unstable; urgency=low

  * Initial Release. (Closes: #584202)

 -- Angel Abad <angelabad@gmail.com>  Thu, 03 Jun 2010 00:29:01 +0200
